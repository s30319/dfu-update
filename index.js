window.onload = function () {
  document.getElementById('test').addEventListener('click', async () => {
    console.log('a')
    window.device = await navigator.usb.requestDevice({ filters: [
      { vendorId: 0x04, productId: 0x04 },
      { vendorId: 4617, productId: 21441 },
      { vendorId: 0x2341, productId: 0x8036 },
      { vendorId: 0x2341, productId: 0x8037 },
      { vendorId: 0x2341, productId: 0x804d },
      { vendorId: 0x2341, productId: 0x804e },
      { vendorId: 0x2341, productId: 0x804f },
      { vendorId: 0x2341, productId: 0x8050 }
    ]})
    await device.open()
    await device.selectConfiguration(1)
    await device.claimInterface(0)
    // const tmp = await send(new Uint8Array([0x01]))
    // await sendControl([0x01])
    let resulst, p
    const res = await getFW().then(res => {
      return res.arrayBuffer()
    }).then(buffer => {
      return new Uint8Array(buffer).buffer
    }).then(buf => {
      p = new SecureDfuPackage(buf)
      p.load().then(() => {
        return p.getBaseImage()
      }).then(image => {
        if (image) {
          update(image.initData, image.imageData)
        }
      }).then(() => {
        return p.getAppImage()
      }).then(image => {
        update(image.initData, image.imageData)      
      })
    })
    // const buffer = await res.arrayBuffer()
    // resulst = new Uint8Array(buffer).buffer
    // let p = new SecureDfuPackage(resulst)
    // p.load()
    // console.log(p)
    // return Promise.resolve().then(() => {
    //   console.log(p.manifest)
    //   if (p.Package.manifest.softdevice) {
    //     return p.getBaseImage()
    //   }
    // // }).then((image) => {
    // //   if (image) {
    // //     return update(image.initData, image.imageData)
    // //   }
    // // }).then(() => {
    // //   if (p.manifest.application) {
    // //     return p.getAppImage()
    // //   }
    // // }).then(image => {
    // //   console.log(image)
    // })
  })
}
const getFW = async () => {
  return fetch('http://localhost:8888/fw/dl', {
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      fileName: ''
    }),
    method: 'POST'
  })
}
const StatusCodes = {
  // FAIL_ERROR: 0x5001,
  // CLS_ERROR: 0x5002,
  // INS_ERROR: 0x5003,
  // OK: 0x9000
  FAIL_ERROR: '5001',
  CLA_ERROR: '5002',
  INS_ERROR: '5003',
  OK: '9000'
};

const mergeTypedArray = (buffer1, buffer2) => {
  // Checks for truthy values on both arrays
  if (!buffer1 && !buffer2) throw new Error('Please specify valid arguments');

  if (!buffer2 || buffer2.length === 0) return buffer1;
  if (!buffer1 || buffer1.length === 0) return buffer2;

  const tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
  tmp.set(new Uint8Array(buffer1), 0);
  tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
  return tmp.buffer;
}

const send = async content => {
  await device.transferOut(1, content);
};

// define a function to receive at most 64 bytes data, also return a promise, if occur an error reject it
const recv = async () => {
  const receive = await device.transferIn(1, 64);
  return receive;
};

const toHexString = bytes =>
  bytes.reduce((str, byte) => str + byte.toString(16).padStart(2, '0'), '');

const sendApdu = async (apdu) => {
  // have bug?!! it needs to be true
  // if (!this.device.opened) {
  //   console.log("i haven't connected");
  // await this.connect();
  // }

  const errorApdu = recvView => {
    // FIXME: need to fix to web version
    const dataLen = recvView.getUint16(0, true);
    console.log('recv data length', dataLen);
    const statusView = new Uint8Array(recvView.buffer.slice(2 + dataLen, 2 + dataLen + 2));
    const status = toHexString(statusView);
    if (status !== '9000') {
      throw new Error(status);
    }
    return recvView;
  };

  const transport = apdu;
  const processApdu = () => {
    // define a function to send at most 64 bytes data, and return a promise
    const send = async content => {
      await device.transferOut(1, content);
    };

    // define a function to receive at most 64 bytes data, also return a promise, if occur an error reject it
    const recv = async () => {
      const receive = await device.transferIn(1, 64);
      return receive;
    };

    // here is the function that exchange apdu command
    const exchangeApdu = async () => {
      let offsetSent = 0;
      let firstReceived = true;
      let dataLengthtoReceive = 0;
      let received; // undefined
      const firstDataLengthToReceive = 60;

      // just send part of data if it's more than 64 bytes
      const SendPartOfData = async () => {
        if (offsetSent === transport.byteLength) {
          return receivePartOfData();
        }

        const blockSize = transport.byteLength - offsetSent > 64
          ? 64
          : transport.byteLength - offsetSent;
        let block = transport.slice(offsetSent, offsetSent + blockSize);
        const blankSize = 64 - block.byteLength;

        if (blankSize !== 0) {
          // FIXME: mergedTyped Array 應該要只能吃 arraybuffer params
          const blank = new Uint8Array(blankSize);
          block = mergeTypedArray(block, blank);
        }
        // call send data and recursive to it self, async problem: receive is done before this then function
        // return send(block).then(() => {
        //   offsetSent += blockSize;
        //   console.log(`offset new: ${offsetSent}`);
        //   return SendPartOfData();
        // });
        await send(block);
        offsetSent += blockSize;
        console.log(`offset new: ${offsetSent}`);
        return SendPartOfData();
      };

      // just receive part of data if it's more than 64 bytes
      const receivePartOfData = () => {
        // if it's sucuX device
        if (true) {
          return recv().then(result => {
            console.log('recv result', result);
            if (!received) {
              received = result.data; // dataView
            } else {
              // merged array need to be arraybuffer. ArrayBuffer => DataView
              received = new DataView(mergeTypedArray(received.buffer, result.data.buffer));
            }
            if (firstReceived) {
              firstReceived = false;
              // received is already DataView
              const dataViewReceived = received;
              dataLengthtoReceive = dataViewReceived.getUint16(0, true); // little endian
              console.log('length recv', dataLengthtoReceive);
              // if (dataLengthtoReceive > 128) {
              //   throw new Error('return data too long');
              // }

              if (dataLengthtoReceive <= firstDataLengthToReceive) {
                console.log('data is less than 60 bytes');
                received = errorApdu(received);
                return received;
              }
              dataLengthtoReceive -= firstDataLengthToReceive; // 60
              return receivePartOfData();
            }

            // need to consider status for 2 bytes
            if (dataLengthtoReceive <= 62) {
              console.log('i have receive more');
              received = errorApdu(received);
              return received;
            }
            dataLengthtoReceive -= 64;
            return receivePartOfData();
          });
        }
      };
      return SendPartOfData();
    };

    return exchangeApdu();
  };
  return processApdu();
}

const sendControl = function (operation, buffer) {
  return sendOperation(operation, buffer);
}
const sendOperation = async function (operation, buffer) {
    let size = operation.length;
    if (buffer !== undefined) {
      size += buffer.byteLength;
    }
    const value = new Uint8Array(size);
    value.set(operation);
    if (buffer !== undefined) {
        const data = new Uint8Array(buffer);
        value.set(data, operation.length);
    }
    // if (size < 64) {
    //   const rest = 64 - size
    //   const fillup = Array(rest).fill(0)
    //   value.set(fillup, size)
    // }

    let received
    console.log(value)
    return send(value).then(() => {
      return recv().then(res => {
        console.log(`回來的是: ${res}`)
        if (!received) {
          received = res.data; // dataView
        } else {
          // merged array need to be arraybuffer. ArrayBuffer => DataView
          received = new DataView(mergeTypedArray(received.buffer, result.data.buffer));
        }
        return received
      }).then(view => {
        const result = view.getUint8(2)
        if (result === 0x01) {
          return new DataView(view.buffer, 3)
        } else {
          return new Error('operation fail')
        }
      })
    })
}

const transferInit = function (buffer) {
  return transfer(buffer, "init", [0x06, 0x01], [0x01,0x01]);
}

const transfer = function (buffer, type, selectType, createType) {
  return sendControl(selectType)
  .then(response => {
      console.log(response)
      const maxSize = response.getUint32(0, true);
      const offset = response.getUint32(4, true);
      const crc = response.getInt32(8, true);
      console.log(maxSize)
      if (type === "init" && offset === buffer.byteLength && checkCrc(buffer, crc)) {
          console.log("init packet already available, skipping transfer");
          return;
      }

      // progress = bytes => {
      //     window.dispatchEvent('progress', {
      //         object: type,
      //         totalBytes: buffer.byteLength,
      //         currentBytes: bytes
      //     });
      // };
      // progress(0);
      return transferObject(buffer, createType, maxSize, offset);
  });
}

const transferFirmware = (buffer) => {
  return transfer(buffer, "firmware", [ 0x06, 0x02 ], [ 0x01, 0x02 ]);
}

const transferObject = (buffer, createType, maxSize, offset) => {
  const start = offset - offset % maxSize;
  const end = Math.min(start + maxSize, buffer.byteLength);

  const view = new DataView(new ArrayBuffer(4));
  view.setUint32(0, end - start, true);
  return sendControl(createType, view.buffer)
  .then(() => {
      const data = buffer.slice(start, end);
      return transferData(data, start);
  })
  .then(() => {
      return sendControl([ 0x03 ]);
  //     return send(new Uint8Array([0x03])).then(() => {
  //       return recv()
  //     }).then(res => {
  //       return res.data
  //     }).then(result => {
  //       console.log(`result ${result.getUint8(2)}`)
  //     })
  })
  .then(response => {
      const crc = response.getInt32(4, true);
      const transferred = response.getUint32(0, true);
      const data = buffer.slice(0, transferred);

      if (checkCrc(data, crc)) {
          console.log(`written ${transferred} bytes`);
          offset = transferred;
          return sendControl([ 0x04 ]);
      } else {
          console.log("object failed to validate");
      }
  })
  .then(() => {
      if (end < buffer.byteLength) {
          return transferObject(buffer, createType, maxSize, offset);
      } else {
          console.log("transfer complete");
      }
  });
}

const delayPromise = (delay) => {
  return new Promise(resolve => {
      setTimeout(resolve, delay);
  });
}

const transferData = (data, offset, start) => {
  start = start || 0;
  const end = Math.min(start + 60, data.byteLength);
  const packet = data.slice(start, end);
  const size = packet.byteLength
  const value = new Uint8Array(size + 1)
  value.set([0x08])
  const packData = new Uint8Array(packet)
  value.set(packData, 1) 
  console.log(value)
  return send(value)
  .then(() => delayPromise(3))
  .then(() => {
      console.log(`已傳送: ${offset + end}`);

      if (end < data.byteLength) {
          return transferData(data, offset, end);
      }
  });
}

const checkCrc = function (buffer, crc) {
  console.log(toHexString(new Uint8Array(new Int32Array([crc]).buffer)), " <  > ", toHexString(new Uint8Array(new Int32Array([CRC32.buf(new Uint8Array(buffer))]).buffer)));
  return crc === CRC32.buf(new Uint8Array(buffer));
}

const update = (init, firmware) => {
  return transferInit(init).then(() => {
    console.log('init done')
    return transferFirmware(firmware)
  })
  // const selectType = [ 0x06, 0x01 ]
  // const createType = [ 0x01, 0x01 ]
  // let size = selectType.length
  // const initValue = new Uint8Array(size)
  // value.set(selectType)
  // const initRes = await processApdu(value)
  // const initMaxSize = initRes.getUint32(0, true)
  // const initOffset = initRes.getUint32(4, true)
  // const initCrc = initRes.getUint32(8, true)
  // if (type === "init" && offset === init.byteLength && this.checkCrc(init, crc)) {
  //   this.log("init packet already available, skipping transfer");
  //   return
  // }
  // window.dispatchEvent('progress', {
  //   object: type,
  //   totalBytes: init.byteLength,
  //   currentBytes: 0
  // })
  // const initStart = initOffset - initOffset % initMaxSize
  // const initEnd = Math.min(start + initMaxSize, init)
  // const initView = new DataView(new ArrayBuffer(4))
  // initView.setUint32(0, initEnd - initStart, true)
  // size = createType.length + initView.length
  // const initCreateValue = new Uint8Array(size)
  // initCreateValue.set(createType)
  // let data = new Uint8Array(init)

} 